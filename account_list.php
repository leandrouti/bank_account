<?php
	class AccountBook{

		public static function getAllAccounts(){
			require("../include/conn.php");
			require("account.php");
			$sql = "SELECT * FROM account";
			$res = $pdo->prepare($sql);
			$res->execute();
			$result = $res->fetchAll(PDO::FETCH_ASSOC);

			$account_list = array();
			foreach($result as $account){
					$acc = new Account($account['id']);
					$account_list[] = $acc;
			}
			return $account_list;
		}
	}
?>
