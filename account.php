<?php
	class Account{
		private $acc_name;
		private $total;
		private $id;
		private $update;

		public function __construct($id = ""){
			if(is_numeric($id)){
				$this->getAccInfoById($id);
			}else{
				//needs to add code here for creating new account
			}
		}

		private function getAccInfoById($id){
			require("../include/conn.php");
			$sql = "SELECT * FROM account where id = :id";
			$res = $pdo->prepare($sql);
			$res->bindParam(":id", $id);
			$res->execute();
			$result = $res->fetch(PDO::FETCH_ASSOC);
			if($result){
				$this->setId($result['id']);
				$this->setTotal($result['total']);
				$this->setAccName($result['name']);
				$this->setLastUpdate($result['last_update']);
				return true;
			}else{
				return false;
			}
		}

		private function setId($id){
			$this->id = $id;
			return true;
		}
		public function getId(){
			return ($this->id) ? $this->id : false;
		}

		public function setTotal($total){
			if(!$this->total){
				$this->total = $total;
				return true;
			}else{
				$this->total = $total;
				return $this->update("total", $this->getTotal());

			}
		}
		public function getTotal(){
			return ($this->total) ? $this->total : false;
		}

		private function setLastUpdate($update){
			$this->update = $update;
		}

		public function getToday(){
			return $today = date('Y-m-d');
		}

		public function getLastUpdate(){
			return ($this->update) ? $this->update : false;
		}

		public function setAccName($name){
			$this->acc_name = $name;
			return true;
		}

		public function getAccName(){
			return ($this->acc_name) ? $this->acc_name : false;
		}

		private function update($col, $val){
			require("../include/conn.php");
			$res = $pdo->prepare("update account set {$col} = :{$col}, last_update = :today WHERE id = :id");
			$res->bindParam(":{$col}", $val);
			$res->bindParam(":today", $this->getToday());
			$res->bindParam(":id", $this->getId());
			$res->execute();
			if($res){
				$this->update = $this->getToday();
				return true;
			}else{
				return false;
			}
		}
	}
?>
