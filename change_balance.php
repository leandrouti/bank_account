<?php
	require("account.php");

	if(!empty($_POST['id']) && !empty($_POST['value'])){

		if(is_numeric($_POST['id']) && is_numeric($_POST['value'])){
			$acc = new Account($_POST['id']);
			$acc->setTotal($_POST['value']);

			echo "Values updated: \n";
			echo "ID: " . $acc->getId() . "\n";
			echo "Name: " . $acc->getAccName() . "\n";
			echo "Value: " . $acc->getTotal() . "\n";
			echo "Last Update: " . $acc->getLastUpdate() . "\n";
		}
	}
?>
