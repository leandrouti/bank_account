<!DOCTYPE html>
<html lang="ja">
<head>
	<title>Show all account Information</title>
	<style>
		table{
			margin: 1em;
		}
		table td{
			padding: .5em;
			text-align:center;
		}
	</style>
<body>
<?php
	require("account_list.php");
	$ar_acc = AccountBook::getAllAccounts();
	foreach($ar_acc as $account){
		echo '<table border=1>';
		echo "<tr><th colspan=\"2\">" . $account->getAccName() . "</th></tr>";
		echo "<tr><td>Valor Atual: </td><td><span id=\"span_value\">" . number_format($account->getTotal()) . "</span></td></tr>";
		echo "<tr><td>Ultima Atualizacao: </td><td>" . $account->getLastUpdate() . "</td></tr>";
		$id = $account->getId();
		echo "<tr><td colspan=\"2\">Update Total:<br><input type=\"text\" id=\"total_{$id}\"><button onclick=\"updateAcc({$id})\">Update</button></td></tr>";
		echo "</table>";
	}
 ?>
 <script src="../js/jquery-1.12.0.min.js"></script>
<script>

	function updateAcc(id){
		var value = $("#total_"+id).val();
		if(/\D/.test(value)){
			alert("Only number are accepted");
		}else{
			$.post("change_balance.php", {'id': id, 'value':value})
			.done(function(data){
				alert(data);
			});
		}

		$("#total_"+id).val("");

	}
</script>
</body>
</html>
